
(function(){
	"use strict";

	angular.module("app").factory("todoService", todoService);

	function todoService(){
		return{
			addNewItem,
			incompleteCount,
			warningLevel,
			convertPriority,
			removeTask,
			removeAllCompletedTasks,
			setFilter,
			missDeadline,
			doneAll
		};

		function incompleteCount(items){
			let count = 0;
			angular.forEach(items, (item) => {
				if(!item.done){
					count++;
				}
			});

			return count;
		};

		function warningLevel(items){
			return incompleteCount(items) < 3 ?
				"label-success" :
				"label-warning";
		};

		function addNewItem(items, newItem) {
			if (newItem && newItem.action) {
				items.push({
					"action": newItem.action,
					"done": false,
				});

				newItem.action = "";
			}
		};

		function removeTask(items, task){
			if (task) {
				items.splice(items.indexOf(task), 1);
			}
		};

		function removeAllCompletedTasks(items){
			var index = items.length;
			while (index--) {
    			if (items[index].done){
        			items.splice(index, 1);
    			}
			}
		};

		function convertPriority(priorityNum){
			if (priorityNum <= 1) {
				return "low";
			} else if (priorityNum >=3) {
				return "high";
			} else {
				return "medium";
			}
		};

		function setFilter(model, column){
			if (model.sortType === column) {
				model.sortOrder = !model.sortOrder;
			} else {
				model.sortType = column;
			}
		};

		function missDeadline(deadline){
			if (new Date(deadline) < new Date) {
				return true;
			}
			return false;
		};

		function doneAll(model){
			var isDone = model.doneAll;
			angular.forEach(model.items, (item) =>{
				item.done = isDone;
			});
		}
	};
})();