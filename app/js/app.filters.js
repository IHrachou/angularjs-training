(function(){
	"use strict";

	angular.module("app").filter("checkedItems", checkedItems);

	function checkedItems(){
		return function(items, showComplete){
			let resultArr = [];

			if (angular.isArray(items)) {
				angular.forEach(items, (item) => {
					if(!item.done || showComplete){
						resultArr.push(item);
					}
				});
				return resultArr;
			} else {
				return items;
			}
		};
	};
})();