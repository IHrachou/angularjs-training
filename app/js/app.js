(function(){
	"use strict";

	// app
	angular.module("app",["ngSanitize", "xeditable"])
		.run(runApp)
		.value("model", {
			"user":"Ilya",
			"userPhoto": "images/Ilya_Hrachou.JPG",
			"sortType": "action",
			"sortOrder": false
		});

	function runApp($http, model){
		$http
			.get("todo.json")
			.then(response => {
				model.items = response.data;
				angular.forEach(model.items, (item) => {
					item.deadline = new Date(item.deadline);
				});
			});
	};

	// bootstrap app
	angular.element(document).ready(function(){
		angular.bootstrap(document, ["app"]);
	});
})();