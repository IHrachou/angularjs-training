(function(){
	"use strict";

	angular.module("app")
		.directive("todoList", todoList)
		.directive("header", header);

	function todoList(){
		return {
			restrict: "E",
			templateUrl: "fragments/table.html"
		};
	};

	function header(){
		return {
			restrict: "A",
			templateUrl: 'fragments/page-header.html'
		};
	};
})();