(function(){
	"use strict";

	// app
	angular.module("app",["ngSanitize"])
		.controller("Todo", Todo)
		.directive("todoList", todoList)
		.factory("todoService", todoService)
		.filter("checkedItems", checkedItems)
		.run(runApp)
		.value("model", {
			"user":"Ilya",
			"userPhoto": "images/Ilya_Hrachou.JPG",
			"sortType": "action",
			"sortOrder": false
		});

	function Todo(model, todoService) {
		let $ctrl = this;
		$ctrl.todo = model;
		$ctrl.showComplete = true;
		Object.assign($ctrl, todoService);
	}

	function todoList(){
		return {
			restrict: "E",
			templateUrl: "table.html"
		};
	};

	function todoService(){
		return{
			addNewItem,
			incompleteCount,
			warningLevel,
			convertPriority,
			removeTask,
			removeAllCompletedTasks,
			setFilter,
			missDeadline,
			doneAll
		};

		function incompleteCount(items){
			let count = 0;
			angular.forEach(items, (item) => {
				if(!item.done){
					count++;
				}
			});

			return count;
		};

		function warningLevel(items){
			return incompleteCount(items) < 3 ?
				"label-success" :
				"label-warning";
		};

		function addNewItem(items, newItem) {
			if (newItem && newItem.action) {
				items.push({
					"action": newItem.action,
					"done": false,
				});

				newItem.action = "";
			}
		};

		function editItem(item) {
			
		};

		function removeTask(items, task){
			if (task) {
				items.splice(items.indexOf(task), 1);
			}
		};

		function removeAllCompletedTasks(items){
			var index = items.length;
			while (index--) {
    			if (items[index].done){
        			items.splice(index, 1);
    			}
			}
		};

		function convertPriority(priorityNum){
			if (priorityNum <= 1) {
				return "low";
			} else if (priorityNum >=3) {
				return "high";
			} else {
				return "medium";
			}
		};

		function setFilter(model, column){
			if (model.sortType === column) {
				model.sortOrder = !model.sortOrder;
			} else {
				model.sortType = column;
			}
		};

		function missDeadline(deadline){
			if (new Date(deadline) < new Date) {
				return true;
			}
			return false;
		};

		function doneAll(model){
			var isDone = model.doneAll;
			angular.forEach(model.items, (item) =>{
				item.done = isDone;
			});
		}
	};

	function checkedItems(){
		return function(items, showComplete){
			let resultArr = [];

			if (angular.isArray(items)) {
				angular.forEach(items, (item) => {
					if(!item.done || showComplete){
						resultArr.push(item);
					}
				});
				return resultArr;
			} else {
				return items;
			}
		};
	};

	function runApp($http, model){
		$http
			.get("todo.json")
			.then(response => model.items = response.data);
	};

	// bootstrap app
	angular.element(document).ready(function(){
		angular.bootstrap(document, ["app"]);
	});
})();